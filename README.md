# Dockerfiles

A collection of custom Dockerfiles created for personal use. Attribution information is contained in the Dockerfile itself and any specific information will be located in that project's README
