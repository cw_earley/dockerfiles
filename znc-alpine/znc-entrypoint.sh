#! /usr/bin/env sh

# Options.
DATADIR="/etc/znc-data"

# Create default config if it doesn't exist
if [ ! -f "${DATADIR}/configs/znc.conf" ]; then
  mkdir -p "${DATADIR}/configs"
  cp /znc.conf.default "${DATADIR}/configs/znc.conf"
fi

if [ ! -f "${DATADIR}/znc.pem" ]; then
  znc --datadir="$DATADIR" --makepem
fi

chown -R znc:znc "$DATADIR"

# Start ZNC.
exec s6-setuidgid znc znc --foreground --datadir="$DATADIR" $@